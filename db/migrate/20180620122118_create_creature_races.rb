class CreateCreatureRaces < ActiveRecord::Migration[5.0]
  def change
    create_table :creature_races do |t|
      t.string :name
      t.integer :pos

      t.timestamps
    end
  end
end
