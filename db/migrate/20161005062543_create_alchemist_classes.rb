class CreateAlchemistClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :alchemist_classes, id: false do |t|
      t.string :rank, limit: 20, null: false
      t.string :el, limit: 3, primary_key: true, null: false
      t.integer :al, null: false
      t.integer :ep, null: false
      t.integer :mi, null: false
      t.string :enc, limit: 5, null: false
      t.integer :kp, null: false
   
      t.integer :ip, null: false
      t.integer :q1, null: false
      t.integer :q2, null: true
      t.integer :q3, null: true
      t.integer :q4, null: true
    end
  end
end
