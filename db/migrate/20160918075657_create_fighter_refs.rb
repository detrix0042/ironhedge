class CreateFighterRefs < ActiveRecord::Migration[5.0]
  def change
    create_table :fighter_classes, id: false do |t|
      t.string :rank, limit: 20, null: false
      t.string :el, limit: 3, primary_key: true, null: false
      t.integer :al, null: false
      t.integer :ep, null: false
      t.integer :ms, null: false
      t.string :enc, limit: 5, null: false
      t.integer :kp, null: false
      t.integer :at, null: false
    end
  end
end
