class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :uname, limit: 50, unique: true, index: true, null: false
      t.string :email, limit: 80, index: true
      t.string :password
      t.string :password_digest

      t.timestamps
    end
  end
end
