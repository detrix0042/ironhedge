class CreateGroupsUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :groups_users do |t|
      t.integer :group_id, index: true
      t.integer :user_id, index: true
    end
  end
end
