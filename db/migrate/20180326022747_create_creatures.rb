class CreateCreatures < ActiveRecord::Migration[5.0]
  def change
    create_table :creatures do |t|
      t.string :race, limit: 20
      t.integer :position
      t.string :klass, limit: 30
      t.string :numof, limit: 10
      t.string :elal, limit: 10
      t.integer :lp
      t.string :af, limit: 20
      t.string :df, limit: 2
      t.string :armor, limit: 2
      t.integer :numatt
      t.integer :kp
      t.integer :mph
      t.string :money, limit: 10
      t.string :special, limit: 4
      t.string :jewel, limit: 4
      t.integer :Str
      t.integer :Int
      t.integer :Dex

      t.timestamps
    end
  end
end
