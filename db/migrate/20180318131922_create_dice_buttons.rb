class CreateDiceButtons < ActiveRecord::Migration[5.0]
  def change
    create_table :dice_buttons do |t|
      t.string :label
      t.integer :pos
    end
  end
end
