class CreateThiefClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :thief_classes, id: false do |t|
      t.string :rank, limit: 20, null: false
      t.string :el, primary_key: true, null: false, unique: true
      t.integer :al, null: false
      t.integer :ep, null: false
      t.integer :md, null: false
      t.string :enc, limit: 5, null: false
      t.integer :kp, null: false
      t.integer :cw, limit: 2, null: false
      t.integer :pl, limit: 2, null: false
      t.integer :nt, limit: 2, null: false
      t.integer :ps, limit: 2, null: false
      t.integer :mq, limit: 2, null: false
      t.integer :mu, limit: 2, null: false
      t.integer :hn, limit: 2, null: false
    end
  end
end
