# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require_relative 'encounterdb.rb'

$stdout.sync = true
encdb = EncounterDB.new

if Creature.count == 0
  encdb.each do |r, klasses|
    klasses.each do |k, data|
      Creature.create( {
        race: r,
        klass: k,
        numof:       data[0],
        position:    data[1],
        elal:        data[2],
        lp:          data[3],
        af:          data[4],
        df:          data[5],
        armor:       data[6],
        numatt:      data[7],
        kp:          data[8],
        mph:         data[9],
        money:      data[10],
        jewel:      data[11],
        special:    data[12],
        Str:        data[13],
        Int:        data[14],
        Dex:        data[15]
      })
    end
  end
end

races = [
  'Men',
  'Dwarf',
  'Elf',
  'Menlike',
  'Giants',
  'Riding Animals',
  'Wild Animals',
  'Game Animals',
  'Reptiles',
  'Pests',
  'Spirits',
  'Undead',
  'Sprites',
  'Elementals',
  'Silicon'
]
if CreatureRace.count == 0
  puts "seeding CreatureRace table\n"
  races.each_with_index {| race, idx |
    CreatureRace.create name: race, pos: idx
  }
  puts "done.\n"
end

buttons = [
  { label: 'D4', pos: '1' },
  { label: '2D4', pos: '2' },
  { label: '3D4', pos: '3' },
  { label: '5D4', pos: '4' },
  { label: 'D6', pos: '5' },
  { label: '2D6', pos: '6' },
  { label: '3D6', pos: '7' },
  { label: '5D6', pos: '8' },
  { label: 'D8', pos: '9' },
  { label: '2D8', pos: '10' },
  { label: '3D8', pos: '11' },
  { label: '5D8', pos: '12' },
  { label: 'D10', pos: '13' },
  { label: '2D10', pos: '14' },
  { label: '3D10', pos: '15' },
  { label: '5D10', pos: '16' },
  { label: '5D10', pos: '17' },
  { label: 'D12', pos: '18' },
  { label: 'D20', pos: '19' },
  { label: 'D30', pos: '20' },
  { label: '%D', pos: '21' },
]

if DiceButton.count == 0
  puts "Adding button labels"
  for btn in buttons
    DiceButton.create btn
  end
end


groups = [ {name: 'admin'}, {name: 'user'}]
if Group.count == 0
  print "Adding groups to DB"
  groups.each do |g|
    Group.create(g)
    print "."
  end
  print "..done\n"
end

if User.count == 0
  print "adding admin user"
  admin = User.create(uname: 'admin', email: 'admin@admin.net', password: 'spiderman', password_confirmation: 'spiderman')
  print "....done\n"
else
  admin = User.find_by_uname('admin')
end

groupsarray = admin.groups.collect { |g| g.name }
if !groupsarray.include? "admin"
  print "adding admin group to admin user"
  ag = Group.find_by_name('admin')
  # since User.groups is a many to many relationship User.groups is an array
  if admin.present? and ag.present?
    admin.groups << ag # this appends the group to the User.groups array and saves it.
  end
  print "....done\n"
end

def addFighterClasses()
  classes =  [{ rank: 'Warrior',          el: 'F1', al: 1, ep:     0, ms:  2, enc: '01-30', kp:  6, at: 1},
              { rank: 'Master Warrior',   el: 'F2', al: 2, ep:    20, ms:  4, enc: '31-60', kp:  7, at: 1},
              { rank: 'Gladiator',        el: 'F3', al: 3, ep:    60, ms:  6, enc: '61-80', kp:  9, at: 1},
              { rank: 'Master Gladiator', el: 'F4', al: 4, ep:   200, ms:  8, enc: '81-90', kp: 14, at: 2},
              { rank: 'Centurion',        el: 'F5', al: 5, ep:  1200, ms: 10, enc: '91-97', kp: 20, at: 2},
              { rank: 'Commander',        el: 'F6', al: 6, ep:  4000, ms: 12, enc: '98-99', kp: 35, at: 3},
              { rank: 'General',          el: 'F7', al: 7, ep: 10000, ms: 13, enc:  '100',  kp: 50, at: 3}
              ]
  classes.each do |fc|
    FighterClass.create(fc)
    print '.'
  end # fclasses.each
end # addFigtherClasses

def addThiefClasses()
  classes = [
    { rank: 'Shoplifter',   el: 'T1', al: 1, ep:    0, md:  5, enc: '01-40', kp:  4, cw: 30, pl: 25, nt: 20, ps: 15, mq: 17, mu: 25, hn: 15 },
    { rank: 'Robber',       el: 'T2', al: 2, ep:   30, md:  7, enc: '41-70', kp:  5, cw: 45, pl: 40, nt: 36, ps: 30, mq: 33, mu: 38, hn: 25 },
    { rank: 'Burglar',      el: 'T3', al: 3, ep:  100, md:  9, enc: '71-90', kp:  7, cw: 60, pl: 52, nt: 49, ps: 45, mq: 50, mu: 50, hn: 45 },
    { rank: 'Thief',        el: 'T4', al: 4, ep:  500, md: 11, enc: '91-97', kp: 10, cw: 70, pl: 64, nt: 62, ps: 60, mq: 67, mu: 62, hn: 65 },
    { rank: 'Master Thief', el: 'T5', al: 5, ep: 1800, md: 12, enc: '98-99', kp: 15, cw: 79, pl: 76, nt: 75, ps: 73, mq: 78, mu: 74, hn: 80 },
    { rank: 'Expert Thief', el: 'T6', al: 6, ep: 7500, md: 13, enc:  '100',  kp: 25, cw: 87, pl: 87, nt: 87, ps: 85, mq: 86, mu: 85, hn: 90 }
  ]

  classes.each do |c|
    ThiefClass.create(c)
    print '.'
  end # tclasses.each
end # addThiefClasses

def addMagicianClasses()
  classes = [
    { rank: 'Conjurer',    el: 'M1', al: 1, ep:     1, mi:  5, enc: '01-40', kp:   3, dm: 10, q1: 1, q2: nil, q3: nil, q4: nil },
    { rank: 'Enchanter',   el: 'M2', al: 1, ep:    20, mi:  7, enc: '41-80', kp:   6, dm: 30, q1: 2, q2: nil, q3: nil, q4: nil },
    { rank: 'Sorcerer',    el: 'M3', al: 2, ep:    70, mi:  9, enc: '81-90', kp:  10, dm: 50, q1: 3, q2:   1, q3: nil, q4: nil },
    { rank: 'Necromancer', el: 'M4', al: 2, ep:   250, mi: 11, enc: '91-95', kp:  20, dm: 70, q1: 4, q2:   2, q3: nil, q4: nil },
    { rank: 'Wizzard',     el: 'M5', al: 3, ep:  1400, mi: 12, enc: '96-97', kp:  50, dm: 85, q1: 5, q2:   3, q3:   1, q4: nil },
    { rank: 'Wizzard II',  el: 'M6', al: 3, ep:  5000, mi: 12, enc: '98',    kp: 100, dm: 90, q1: 6, q2:   4, q3:   2, q4: nil },
    { rank: 'Wizzard III', el: 'M7', al: 3, ep: 15000, mi: 12, enc: '99',    kp: 250, dm: 93, q1: 6, q2:   5, q3:   3, q4:   1 },
    { rank: 'Wizzard IV',  el: 'M8', al: 3, ep: 30000, mi: 12, enc: '100',   kp: 500, dm: 95, q1: 6, q2:   5, q3:   4, q4:   2 }
  ]

  classes.each do |c|
    MagicianClass.create(c)
    print '.'
  end # mclasses.each
end # addMagicianClasses

def addAlchemistClasses()
  classes = [
    { rank: 'Mixer',       el: 'A1', al: 1, ep:     1, mi:  5, enc: '01-30', kp:   4, ip: 10, q1: 1, q2: nil, q3: nil, q4: nil },
    { rank: 'Potioner',    el: 'A2', al: 1, ep:    20, mi:  7, enc: '31-70', kp:   5, ip: 30, q1: 2, q2: nil, q3: nil, q4: nil },
    { rank: 'Concocter',   el: 'A3', al: 2, ep:    50, mi:  9, enc: '71-90', kp:   7, ip: 50, q1: 3, q2:   1, q3: nil, q4: nil },
    { rank: 'Chemist',     el: 'A4', al: 2, ep:   200, mi: 11, enc: '91-97', kp:  10, ip: 70, q1: 4, q2:   2, q3: nil, q4: nil },
    { rank: 'Chemist II',  el: 'A5', al: 3, ep:  1300, mi: 12, enc: '98-99', kp:  13, ip: 90, q1: 5, q2:   3, q3:   1, q4: nil },
    { rank: 'Chemist III', el: 'A6', al: 3, ep:  6500, mi: 12, enc: '100',   kp:  20, ip: 95, q1: 6, q2:   4, q3:   2, q4: nil }
  ]

  classes.each do |c|
    AlchemistClass.create(c)
    print '.'
  end # classes.each
end # addAlchemistClasses

def addPriestClasses()
  classes = [
    { rank: 'Follower',     el: 'P1', al: 1, ep:     0, mi:  2, enc: '01-40', kp:  -3, di: 10, q1: 1, q2: nil, q3: nil, q4: nil },
    { rank: 'Disciple',     el: 'P2', al: 1, ep:    20, mi:  4, enc: '41-70', kp:  -5, di: 25, q1: 2, q2: nil, q3: nil, q4: nil },
    { rank: 'Monk',         el: 'P3', al: 1, ep:   100, mi:  6, enc: '71-90', kp:  -7, di: 40, q1: 3, q2:   1, q3: nil, q4: nil },
    { rank: 'Elder Monk',   el: 'P4', al: 1, ep:   400, mi:  8, enc: '91-95', kp: -10, di: 60, q1: 4, q2:   2, q3: nil, q4: nil },
    { rank: 'Priest',       el: 'P5', al: 1, ep:  1500, mi: 10, enc: '96-97', kp: -40, di: 80, q1: 5, q2:   3, q3:   1, q4: nil },
    { rank: 'Chief Priest', el: 'P6', al: 1, ep:  7000, mi: 12, enc: '98-99', kp:-100, di: 90, q1: 6, q2:   4, q3:   2, q4: nil },
    { rank: 'High Priest',  el: 'P7', al: 1, ep: 20000, mi: 13, enc: '100',   kp:-250, di: 95, q1: 6, q2:   5, q3:   2, q4: 1 }
  ]

  classes.each do |c|
    PriestClass.create(c)
    print '.'
  end # classes.each
end # addPriestClasses

if FighterClass.count == 0
  print "FighterClass table empty: Adding Fighter Classes"
  addFighterClasses
elsif FighterClass.count < 7 # count is not zero and is less than 7 suggests that something went wrong(maybe)
  print "\nFighterClass table not complete, deleting whats there and adding fighter classes"
  FighterClass.delete_all    # delete all and just populate it with data above
  addFighterClasses
end

if ThiefClass.count == 0
  print "ThiefClass table empty: Adding Thief Classes"
  addThiefClasses
elsif ThiefClass.count < 6
  print "ThiefClass table not complete, deleting whats there and adding thief classes"
  ThiefClass.delete_all
  addThiefClasses
end # ThiefClass.count

if MagicianClass.count == 0
  print "MagicianClass table empty: Adding Magician Classes"
  addMagicianClasses
elsif MagicianClass.count < 8
  print "MagicianClass table not complete, deleting whats there and adding Magician classes"
  MagicianClass.delete_all
  addMagicianClasses
end # MagicianClass.count

if AlchemistClass.count == 0
  print "AlchemistClass table empty: Adding Alchemist Classes"
  addAlchemistClasses
elsif AlchemistClass.count < 6
  print "AlchemistClass table not complete, deleting whats there and adding Alchemist classes"
  AlchemistClass.delete_all
  addAlchemistClasses
end # AlchemistClass.count

if PriestClass.count == 0
  print "PriestClass table empty: Adding Priest Classes"
  addPriestClasses
elsif PriestClass.count < 7
  print "PriestClass table not complete, deleting whats there and adding Priest classes"
  PriestClass.delete_all
  addPriestClasses
end # PriestClass.count
print '\n'