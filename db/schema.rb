# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180620122118) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alchemist_classes", primary_key: "el", id: :string, limit: 3, force: :cascade do |t|
    t.string  "rank", limit: 20, null: false
    t.integer "al",              null: false
    t.integer "ep",              null: false
    t.integer "mi",              null: false
    t.string  "enc",  limit: 5,  null: false
    t.integer "kp",              null: false
    t.integer "ip",              null: false
    t.integer "q1",              null: false
    t.integer "q2"
    t.integer "q3"
    t.integer "q4"
  end

  create_table "creature_races", force: :cascade do |t|
    t.string   "name"
    t.integer  "pos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "creature_types", force: :cascade do |t|
    t.string "name", limit: 20
    t.index ["name"], name: "index_creature_types_on_name", using: :btree
  end

  create_table "creatures", force: :cascade do |t|
    t.string   "race",       limit: 20
    t.integer  "position"
    t.string   "klass",      limit: 30
    t.string   "numof",      limit: 10
    t.string   "elal",       limit: 10
    t.integer  "lp"
    t.string   "af",         limit: 20
    t.string   "df",         limit: 2
    t.string   "armor",      limit: 2
    t.integer  "numatt"
    t.integer  "kp"
    t.integer  "mph"
    t.string   "money",      limit: 10
    t.string   "special",    limit: 4
    t.string   "jewel",      limit: 4
    t.integer  "Str"
    t.integer  "Int"
    t.integer  "Dex"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "dice_buttons", force: :cascade do |t|
    t.string  "label"
    t.integer "pos"
  end

  create_table "fighter_classes", primary_key: "el", id: :string, limit: 3, force: :cascade do |t|
    t.string  "rank", limit: 20, null: false
    t.integer "al",              null: false
    t.integer "ep",              null: false
    t.integer "ms",              null: false
    t.string  "enc",  limit: 5,  null: false
    t.integer "kp",              null: false
    t.integer "at",              null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name",       limit: 20, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["name"], name: "index_groups_on_name", using: :btree
  end

  create_table "groups_users", force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.index ["group_id"], name: "index_groups_users_on_group_id", using: :btree
    t.index ["user_id"], name: "index_groups_users_on_user_id", using: :btree
  end

  create_table "magician_classes", primary_key: "el", id: :string, limit: 3, force: :cascade do |t|
    t.string  "rank", limit: 20, null: false
    t.integer "al",              null: false
    t.integer "ep",              null: false
    t.integer "mi",              null: false
    t.string  "enc",  limit: 5,  null: false
    t.integer "kp",              null: false
    t.integer "dm",              null: false
    t.integer "q1",              null: false
    t.integer "q2"
    t.integer "q3"
    t.integer "q4"
  end

  create_table "priest_classes", primary_key: "el", id: :string, limit: 3, force: :cascade do |t|
    t.string  "rank", limit: 20, null: false
    t.integer "al",              null: false
    t.integer "ep",              null: false
    t.integer "mi",              null: false
    t.string  "enc",  limit: 5,  null: false
    t.integer "kp",              null: false
    t.integer "di",              null: false
    t.integer "q1",              null: false
    t.integer "q2"
    t.integer "q3"
    t.integer "q4"
  end

  create_table "saving_rolls", force: :cascade do |t|
    t.string  "c0"
    t.integer "c1"
    t.integer "c2"
    t.integer "c3"
    t.integer "c4"
    t.integer "c5"
  end

  create_table "thief_classes", primary_key: "el", id: :string, force: :cascade do |t|
    t.string  "rank", limit: 20, null: false
    t.integer "al",              null: false
    t.integer "ep",              null: false
    t.integer "md",              null: false
    t.string  "enc",  limit: 5,  null: false
    t.integer "kp",              null: false
    t.integer "cw",   limit: 2,  null: false
    t.integer "pl",   limit: 2,  null: false
    t.integer "nt",   limit: 2,  null: false
    t.integer "ps",   limit: 2,  null: false
    t.integer "mq",   limit: 2,  null: false
    t.integer "mu",   limit: 2,  null: false
    t.integer "hn",   limit: 2,  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "uname",           limit: 50, null: false
    t.string   "email",           limit: 80
    t.string   "password"
    t.string   "password_digest"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["uname"], name: "index_users_on_uname", using: :btree
  end

end
