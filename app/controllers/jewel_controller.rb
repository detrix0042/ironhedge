require 'gamelib/jewel'

class JewelController < ApplicationController
  def cut
    j = Jewel.new params[:str].to_i
    
    render json: j.to_json
  end
end
