class AdminController < ApplicationController
  before_action :isAdmin?

  def index
    @user = User.find_by_id(session[:user_id])
    @users = User.alphasort
    @groups = Group.alphasort
  end

  def fighterClassTable 
    @fclasses = FighterClass.all.sort
    @fc = FighterClass.new
    @floop = true
  end

  def thiefClassTable
    @tclasses = ThiefClass.all.sort
    @tcnew = ThiefClass.new
    @floop = true
  end

  def magicianClassTable
    @mclasses = MagicianClass.all.sort
    @mcnew = MagicianClass.new
    @floop = true
  end

  def alchemistClassTable
    @aclasses = AlchemistClass.all.sort
    @acnew = AlchemistClass.new
    @floop = true
  end

  def priestClassTable
    @pclasses = PriestClass.all.sort
    @pcnew = PriestClass.new
    @floop = true
  end

end
