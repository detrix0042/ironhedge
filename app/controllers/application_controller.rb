class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :isAdmin?

  protected
    def isAdmin?
        if session[:user_id].present?
          user = User.find_by_id(session[:user_id])
          gs = user.groups.collect { |g| g.name }
          return gs.include? "admin"
        else
          flash[:error] = "You are not an Admin. Access denied!"
          redirect_to root_path
        end
    end

end
