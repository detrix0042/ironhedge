require 'gamelib/dice'

class DiceController < ApplicationController
  def roll
    d = Dice.new(params[:str])
    render json: d.to_json
  end
  
  def fetchDiceBtns
    btns = DiceButton.sortpos
    render json: btns
  end
end
