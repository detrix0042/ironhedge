class CreaturesController < ApplicationController
  def fetch_all
    @raceList = CreatureRace.all.collect { |r| r.name }
    
    
    @creatures = {}
    logger.debug "raceList loop:"
    @raceList.each do | race |
      logger.debug "Race: #{race}"
      cs = Creature.where(race: race)
      @creatures[race.to_sym] = {}
      cs.each do |data|
        @creatures[race.to_sym][data.klass] = {
            numof: data.numof,
            elal: data.elal,
            lp: data.lp,
            af: data.af,
            df: data.df,
            armor: data.armor,
            numatt: data.numatt,
            kp: data.kp,
            mph: data.mph,
            money: data.money,
            special: data.special,
            jewel: data.jewel,
            Str: data.Str,
            Int: data.Int,
            Dex: data.Dex
        }
        
      end
    end
    render partial: "creatures/select_panel"
  end


  def fetch_creature
    klass = params[:klass]
    @creature = Creature.find_by_klass(klass)
    
    
  end
  
  def fetch_racelist
    render json: CreatureRace.all.collect { |r| r.name }
  end
  
end
