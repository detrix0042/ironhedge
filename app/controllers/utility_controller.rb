class UtilityController < ApplicationController

  def showEditUser
    @user = User.find_by_id(params[:id])
    ugroups = @user.groups.collect { |g| g.name }

    @otherGroups = Group.alphasort.collect { |g| g.name }

    @otherGroups -= @user.groups.collect { |g| g.name }
    respond_to do |fmt|
      fmt.js
    end
  end

  def updateUser
    @user = User.find_by_id(params[:id])
    @user.update_attributes(user_params)
    respond_to do |fmt|
      fmt.js { render 'utility/finishEditUser' }
    end
  end

  def fetcthUserList
    users = User.alphasort
    respond_to do |fmt|
      fmt.html { render json: users.as_json }
    end
  end

  def fetchGroupList
    groups = Group.alphasort
        respond_to do |fmt|
      fmt.html { render json: groups.as_json }
    end
  end

  def removeUser
    user = User.destroy(params[:id])
    if user
      render plain: "deleted"
    else
      render plain: "failed"
    end

  end

  def showlogin
    respond_to do |fmt|
      fmt.js
    end
  end

  def addNewGroup
    g = Group.create({name: params[:groupname]})
    groups = Group.alphasort

    render json: groups.as_json
  end

  def removeGroup
    g = Group.destroy(params[:id])
    if g
      logger.debug "==============  group #{g.name} deleted successfully."
      render plain: 'deleted'
    else
      logger.debug "==============  Error deleting group #{g.name}"
      render nil
    end

  end

  def addGroupToUser
    @user = User.find_by_id(params[:userid])

    group = Group.find_by_id(params[:gid])

    groups = Group.all.collect {|g| g.name} #get a list of all groups

    @user.groups << group
    @otherGroups = groups - @user.groups.collect { |g| g.name }

    respond_to do |fmt|
      fmt.js { render 'updateGroupLists' }
    end
  end

  def removeGroupFromUser
    @user = User.find_by_id(params[:userid])

    group = Group.find_by_id(params[:gid])
    @user.groups -= [group]
    ugs = @user.groups.collect { |g| g.name }

    logger.debug("==========================")
    logger.debug("@user.groups[]   " + ugs.inspect)
      
    groups = Group.all.collect {|g| g.name} #get a list of all groups

    @otherGroups = groups - @user.groups.collect { |g| g.name }

    respond_to do |fmt|
      fmt.js { render 'updateGroupLists' }
    end
  end
#==============================================================================================
#         Fighter

  def fetchFighterAdminTable
    @fclasses = FighterClass.all.sort
    @fc = FighterClass.new
    @floop = true
    render partial: 'admin/fighterAdminTable.html'
  end

  def addFighterClass
    @fc = FighterClass.new(fc_params)
    cnt = FighterClass.count
    if @fc.save
      @stripe = (cnt % 2 == 0) ? "oddrow" : "evenrow"
      @floop = false
      logger.debug "====> adding new figther class"
      respond_to do |fmt|
        fmt.js { render 'utility/newFighterClass' }
      end
    else
      render 'admin/fighterClassTable'
    end
  end

  def editFighterClass
    @fc = FighterClass.find_by_el(params[:id])
    q = @fc.el[1].to_i # get single character converted to integer to determine odd or even
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    respond_to do |fmt|
      fmt.js { render 'admin/fighter/editInline' }
    end
  end

  def updateFighterClass
    @fc = FighterClass.find_by_el(params[:id])
    @fc.update_attributes(fc_params)
    q = @fc.el[1].to_i # get single character converted to integer to determine odd or even
    @floop = false
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    logger.debug("=====> #{@fc.inspect}")

    respond_to do |fmt|
      fmt.js { render 'admin/fighter/updateClass' }
    end
  end

  def deleteIronClass
    type = params[:type]
    rank = params[:rank]
    logger.debug("=======> made it to deleteIronClass" )
    logger.debug "   type= #{type}"
    logger.debug "   rank= #{rank}"
    klass = nil
    case type.downcase
      when "fighter"
        logger.debug("    deleting Fighter:#{rank}")
        klass = FighterClass.find_by_rank(rank).destroy()
      when "thief"
        logger.debug("    deleting Thief:#{rank}")
        klass = ThiefClass.find_by_rank(rank).destroy()
      when "magician"
        logger.debug("    deleting Magician:#{rank}")
        klass = MagicianClass.find_by_rank(rank).destroy()
      when "alchemist"
        logger.debug("    deleting Alchemist:#{rank}")
        klass = AlchemistClass.find_by_rank(rank).destroy()
      when "priest"
        logger.debug("    deleting Priest:#{rank}")
        klass = PriestClass.find_by_rank(rank).destroy()
      else
        logger.debug("    did not delete any Class")
    end
    respond_to do |fmt|
      fmt.html { 
        if klass
          render json: { 
              ack: "delete successful",
              elid: "row#{klass.el}"
            },
              status: :ok 
        end
      }
    end

  end
#===========================================================================================
#             Thief

  def fetchThiefAdminTable
    @tclasses = ThiefClass.all.sort
    @tcnew = ThiefClass.new
    @floop = true
    render partial: 'admin/thiefAdminTable.html'
  end

  def addThiefClass
    @tc = ThiefClass.new(tc_params)
    cnt = ThiefClass.count
    if @tc.save
      @stripe = (cnt % 2 == 0) ? "oddrow" : "evenrow"
      @floop = false
      logger.debug "====> adding new Thief class"
      respond_to do |fmt|
        fmt.js { render 'utility/newThiefClass' }
      end
    else
      render 'thiefClassTable'
    end
  end

  def editThiefClass
    @tc = ThiefClass.find_by_el(params[:id])
    q = @tc.el[1].to_i # get single character converted to integer to determine odd or even
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    respond_to do |fmt|
      fmt.js { render 'admin/thief/editInline' }
    end
  end

  def updateThiefClass
    @tc = ThiefClass.find_by_el(params[:id])
    @tc.update_attributes(tc_params)
    q = @tc.el[1].to_i # get single character converted to integer to determine odd or even
    @floop = false
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    logger.debug("=====> #{@tc.inspect}")

    respond_to do |fmt|
      fmt.js { render 'admin/thief/updateClass' }
    end
  end
#====================================================================================================
#          Magician

  def fetchMagicianAdminTable
    @mclasses = MagicianClass.all.sort
    @mcnew = MagicianClass.new
    @floop = true
    render partial: 'admin/magicianAdminTable.html'
  end

  def addMagicianClass
    @mcnew = MagicianClass.new(mc_params)
    cnt = MagicianClass.count
    if @mcnew.save
      @stripe = (cnt % 2 == 0) ? "oddrow" : "evenrow"
      @floop = false
      logger.debug "====> adding new Magician class"
      respond_to do |fmt|
        fmt.js { render 'utility/newMagicianClass' }
      end
    else
      render admin_magicianClassTable_path
    end
  end

  def editMagicianClass
    @mc = MagicianClass.find_by_el(params[:id])
    q = @mc.el[1].to_i # get single character converted to integer to determine odd or even
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    respond_to do |fmt|
      fmt.js { render 'admin/magician/editInline' }
    end
  end

  def updateMagicianClass
    @mc = MagicianClass.find_by_el(params[:id])
    @mc.update_attributes(mc_params)
    q = @mc.el[1].to_i # get single character converted to integer to determine odd or even
    @floop = false
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    logger.debug("=====> #{@mc.inspect}")

    respond_to do |fmt|
      fmt.js { render 'admin/magician/updateClass' }
    end
  end
#=====================================================================================================
#               Alchemist

  def fetchAlchemistAdminTable
    @aclasses = AlchemistClass.all.sort
    @acnew = AlchemistClass.new
    @floop = true
    render partial: 'admin/alchemistAdminTable.html'
  end

  def addAlchemistClass
    @acnew = AlchemistClass.new(ac_params)
    cnt = AlchemistClass.count
    if @acnew.save
      @stripe = (cnt % 2 == 0) ? "oddrow" : "evenrow"
      @floop = false
      logger.debug "====> adding new Alchemist class"
      respond_to do |fmt|
        fmt.js { render 'utility/newAlchemistClass' }
      end
    else
      render admin_alchemistClassTable_path
    end
  end

  def editAlchemistClass
    @ac = AlchemistClass.find_by_el(params[:id])
    q = @ac.el[1].to_i # get single character converted to integer to determine odd or even
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    respond_to do |fmt|
      fmt.js { render 'admin/alchemist/editInline' }
    end
  end

  def updateAlchemistClass
    @ac = AlchemistClass.find_by_el(params[:id])
    @ac.update_attributes(ac_params)
    q = @ac.el[1].to_i # get single character converted to integer to determine odd or even
    @floop = false
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    logger.debug("=====> #{@ac.inspect}")

    respond_to do |fmt|
      fmt.js { render 'admin/alchemist/updateClass' }
    end
  end
#================================================================================================
#        Priest

  def fetchPriestAdminTable
    @pclasses = PriestClass.all.sort
    @pcnew = PriestClass.new
    @floop = true
    render partial: 'admin/priestAdminTable.html'
  end

  def addPriestClass
    @pcnew = PriestClass.new(pc_params)
    cnt = PriestClass.count
    if @pcnew.save
      @stripe = (cnt % 2 == 0) ? "oddrow" : "evenrow"
      @floop = false
      logger.debug "====> adding new Priest class"
      respond_to do |fmt|
        fmt.js { render 'utility/newPriestClass' }
      end
    else
      render admin_alchemistClassTable_path
    end
  end

  def editPriestClass
    @pc = PriestClass.find_by_el(params[:id])
    q = @pc.el[1].to_i # get single character converted to integer to determine odd or even
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    respond_to do |fmt|
      fmt.js { render 'admin/priest/editClass' }
    end
  end

  def updatePriestClass
    @pc = PriestClass.find_by_el(params[:id])
    @pc.update_attributes(pc_params)
    q = @pc.el[1].to_i # get single character converted to integer to determine odd or even
    @floop = false
    @stripe = (q % 2 == 1) ? "oddrow" : "evenrow"
    logger.debug("=====> #{@pc.inspect}")

    respond_to do |fmt|
      fmt.js { render 'admin/priest/updateClass' }
    end
  end

#*************************************************************              savingRollsAdminTable
def fetchSavingRollList
  list = SavingRoll.all.to_json
  logger.info "************ fetch Saving Roll List"
  logger.info "#{list.inspect}".html_safe
  render json: list
end

def fetchSavingRollsAdminTable
  render partial: 'admin/savingRollAdminTable'
end  

def srAdd
  row = SavingRoll.create(sr_params)
  logger.info "*********** Saving Roll Add Row"
  logger.info "            #{row.inspect}"
  logger.info "            #{row.to_json.inspect}"
  if row
    render json: row.to_json, response: :ok
  end
end
  
def srDelete
  row = SavingRoll.destroy(params[:srid])
  logger.info "*********** Saving Roll Delete Row"
  logger.info "            params[:srid] = #{params[:srid]}"
  logger.info "            #{row.inspect}"
  logger.info "            #{row.to_json.inspect}"
  if row
    render plain: "delete successful", response: :ok
  end
end

def srEditInline
  @srid = params[:srid]

  render js: 'admin/savingrolls/formInline'
end


#*****************************************************************************************************************
  private
    def fc_params
      params.require(:fighter_class).permit(:rank, :el, :al, :ep, :ms, :enc, :kp, :at)
    end

    def tc_params
      params.require(:thief_class).permit(:rank, :el, :al, :ep, :md, :enc, :kp, :cw, :pl, :nt, :ps, :mq, :mu, :hn)
    end

    def mc_params
      params.require(:magician_class).permit(:rank, :el, :al, :ep, :mi, :enc, :kp, :dm, :q1, :q2, :q3, :q4 )
    end

    def ac_params
      params.require(:alchemist_class).permit(:rank, :el, :al, :ep, :mi, :enc, :kp, :ip, :q1, :q2, :q3, :q4 )
    end

    def pc_params
      params.require(:priest_class).permit(:rank, :el, :al, :ep, :mi, :enc, :kp, :di, :q1, :q2, :q3, :q4 )
    end

    def user_params
      params.require(:user).permit(:uname, :email)
    end

    def sr_params
      params.require(:srAdd).permit(:sid, :c1, :c2, :c3, :c4, :c5)
    end
end
