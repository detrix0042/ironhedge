class MainController < ApplicationController
  

  def index
    @user = nil
    if session[:user_id].present?
      @user = User.find_by_id(session[:user_id])
    end
    
    @diceBtnLbls = DiceButton.sortpos

  end

  def login
    if request.post?
      user = User.find_by_uname(params[:user][:uname])
      if user && user.authenticate(params[:user][:password])
        session[:user_id] = user.id
        redirect_to root_path
      else
        redirect_to root_path, notice: "Authentication Failed", port: 4280
      end
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to root_path, notice: "You are now logged out. Have a great day/night."
  end

  def signup
    if request.post?
      @user = User.new(user_params)
      if @user.save
        session[:user_id] = @user.id
        redirect_to root_url, notice: "#{@user.uname}, Welcome to Novasector"
      else
        render "signup"
      end
    end
  end

  private
    def user_params
      params.require(:user).permit(:uname, :email, :password, :password_confirmation)
    end
    def login_params
      params.require(:user).permit(:uname, :password)
    end
end
