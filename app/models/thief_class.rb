class ThiefClass < ApplicationRecord
  # create_table "thief_classes", primary_key: "el", id: :string, force: :cascade do |t|
  #   t.string  "rank", limit: 2, null: false
  #   t.integer "al",             null: false
  #   t.integer "ep",             null: false
  #   t.integer "md",             null: false
  #   t.string  "enc",  limit: 5, null: false
  #   t.integer "kp",             null: false
  #   t.integer "cw",   limit: 2, null: false
  #   t.integer "pl",   limit: 2, null: false
  #   t.integer "nt",   limit: 2, null: false
  #   t.integer "ps",   limit: 2, null: false
  #   t.integer "mq",   limit: 2, null: false
  #   t.integer "mu",   limit: 2, null: false
  #   t.integer "hn",   limit: 2, null: false
  # end

  self.primary_key = :el

  scope :sort, -> { order("el ASC")}

  validates :rank, presence: true
  validates :el, :al, :ep, :md, :enc, :kp, presence: true

  validates :cw, :pl, :nt, :ps, :mq, :mu, :hn, presence: true
end
