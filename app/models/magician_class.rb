class MagicianClass < ApplicationRecord
  # create_table "magician_classes", primary_key: "el", id: :string, limit: 3, force: :cascade do |t|
  #   t.string  "rank", limit: 20, null: false
  #   t.integer "al",              null: false
  #   t.integer "ep",              null: false
  #   t.integer "mi",              null: false
  #   t.string  "enc",  limit: 5,  null: false
  #   t.integer "kp",              null: false
  #   t.integer "dm",              null: false
  #   t.integer "q1",              null: false
  #   t.integer "q2"
  #   t.integer "q3"
  #   t.integer "q4"
  # end
  self.primary_key = :el

  scope :sort, -> { order("el ASC")}

  validates :rank, presence: true
  validates :el, presence: true
  validates :al, presence: true
  validates :ep, presence: true
  validates :mi, presence: true
  validates :enc, presence: true
  validates :kp, presence: true

  validates :dm, presence: true  
  validates :q1, presence: true  # spell per week quota for level 1
  # q2, q2, and q4 can be nulls so presence is not required


end
