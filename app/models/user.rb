class User < ApplicationRecord
# create_table "users", force: :cascade do |t|
#   t.string   "uname",           limit: 50, null: false
#   t.string   "email",           limit: 80
#   t.string   "password"
#   t.string   "password_digest"
#   t.datetime "created_at",                 null: false
#   t.datetime "updated_at",                 null: false
#   t.index ["email"], name: "index_users_on_email", using: :btree
#   t.index ["uname"], name: "index_users_on_uname", using: :btree
# end

  has_secure_password
  has_and_belongs_to_many :groups

  validates :uname, :email, presence: true

  scope :idsort, -> { order(id: :asc) }
  scope :alphasort, -> { order uname: :asc }
end
