class Group < ApplicationRecord

  has_and_belongs_to_many :users

  scope :idsort, -> { order(id: :asc) }
  scope :alphasort, -> { order name: :asc }
end
