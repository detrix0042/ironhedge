# noinspection RubyTooManyInstanceVariablesInspection
class Creature < ApplicationRecord

  default_scope { order position: :asc }
  scope :races, -> { select(:race).distinct }
end
