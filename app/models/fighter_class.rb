class FighterClass < ApplicationRecord
  # create_table "fighter_class", force: :cascade do |t|
  #     t.string  "rank", limit: 20
  #     t.string  "el",   limit: 3
  #     t.integer "al"
  #     t.integer "ep"
  #     t.integer "ms"
  #     t.string  "enc",  limit: 5
  #     t.integer "kp"
  #     t.integer "at"
  # end
  self.primary_key = :el

  scope :sort, -> { order("el ASC")}

  validates :rank, presence: true
  validates :el, presence: true
  validates :al, presence: true
  validates :ep, presence: true
  validates :ms, presence: true
  validates :enc, presence: true
  validates :kp, presence: true
  validates :at, presence: true

end
