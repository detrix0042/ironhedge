class DiceButton < ApplicationRecord
  
  scope :sortpos, -> { order(pos: :asc) }
end
