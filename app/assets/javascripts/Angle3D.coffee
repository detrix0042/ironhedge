class window.Angle3D
  objName: "Angle3D"
  x: 0
  y: 0
  z: 0

  constructor: (px, y=0.0, z=0.0) ->
    if typeof(px) == 'object'
      @set(px)
    else
      @set(px, y, z)

  set: (px, y=0.0, z=0.0) ->
    if typeof(px) == 'object' and px.objName == 'Point3D'
      @x = px.x
      @y = px.y
      @z = px.z
    else
      @x = px
      @y = y
      @z = z


      
  toString: ->
    "Angle3D:\n" +
      "\t(X:#{@x}, Y:#{@y}, Z:#{@z})"