class window.Polygon
  objName: "Polygon"
  # vlist is list of verticies for this polygon
  # ilist is list of indexes into vlist
  constructor: (ilist, vlist) ->
    #  @PList = [] # 2d projection array
    @PList = []
    @IList = []
    @npoint = 0
    @size = 1
    @color = { red: 0, green: 0, blue: 255 }
    @pixels = [] # single dimentioned array that is the image

    # IList is list of indexes into PList
    # PList is list of verticies that define the shape of this polygon
    @IList = ilist
    @PList = vlist
    
  
    # with PList in place calculate position (center of polygon)
    @pos = @calcPos()

  setSize: (s) ->
    @size = s
    
  # c is a json: { red: 0, green: 0, blue: 0 }
  setColor: (c) ->
    @color.red = c.red
    @color.green = c.green
    @color.blue = c.blue
    
  calcPos: ->
    x=0.0; y=0.0; z=0.0
    for i in @IList
      x += @PList[i].x
      y += @PList[i].y
      z += @PList[i].z

    npts = @IList.length
    # pos is the average of all verticies (should be the center of polygon)
    @pos = new Point3D(x/npts, y/npts, z/npts)
    
  # ctxt is canvas context to draw on.
  draw: (ctx, cam, light, size=1, t={x: 200, y: 200}) ->
    vwidth  = cam.Viewport.width
    vheight = cam.Viewport.height

    plot = []
    vpwc = vwidth/2
    vphc = vheight/2
    for i in @IList
      if @PList[i].z != 0
        pxz = @PList[i].x/@PList[i].z
        pyz = @PList[i].y/@PList[i].z
        x = vpwc + pxz * size * cam.zoom
        y = vphc + pyz * size * cam.zoom
        plot.push( {x: x, y: y} )
      else
        x = vpwc + (@PList[i].x) * size * cam.zoom
        y = vphc + (@PList[i].y) * size * cam.zoom
        plot.push( {x: x, y: y} )
    
    @calcPos()
    pvec1 = Point3D.normalize(@PList[@IList[0]], @PList[@IList[1]])
    pvec2 = Point3D.normalize(@PList[@IList[2]], @PList[@IList[1]])

    pvi = pvec1.y*pvec2.z - pvec1.z*pvec2.y;
    pvj = pvec1.z*pvec2.x - pvec1.x*pvec2.z;
    pvk = pvec1.x*pvec2.y - pvec1.y*pvec2.x;
    
    if light == undefined
      if !!window.jworld
        light = window.jworld.light
      else
        light = new Point3D(0, 0, 10)
    pvector = new Point3D(pvi, pvj, pvk)
    lvector = Point3D.normalize(@PList[@IList[1]], light)
    
    scaler = pvector.x*lvector.x + pvector.y*lvector.y + pvector.z*lvector.z;
    scaler = Math.abs(scaler * 0.79 + 0.2)
    
    red   = Math.floor(@color.red   * scaler)
    green = Math.floor(@color.green * scaler)
    blue  = Math.floor(@color.blue  * scaler)
    
    
    ctx.beginPath()
    ctx.moveTo(plot[0].x, plot[0].y)
    ctx.strokeStyle = "#000"
    for i in [1...@IList.length]
      ctx.lineTo(plot[i].x, plot[i].y)
    ctx.closePath()
    ctx.stroke()
    ctx.fillStyle = "rgb(#{red}, #{green}, #{blue})"
    ctx.fill()

    q=0
    