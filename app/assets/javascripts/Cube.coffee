class window.Cube extends Polyhedron
  objName: 'Cube'
  
  constructor: (ctx, _pos=null) ->
    super(ctx)
    @VList[0] = new Point3D(-0.5,  0.5, -0.5)
    @VList[1] = new Point3D( 0.5,  0.5, -0.5)
    @VList[2] = new Point3D( 0.5, -0.5, -0.5)
    @VList[3] = new Point3D(-0.5, -0.5, -0.5)
    @VList[4] = new Point3D( 0.5,  0.5,  0.5)
    @VList[5] = new Point3D(-0.5,  0.5,  0.5)
    @VList[6] = new Point3D(-0.5, -0.5,  0.5)
    @VList[7] = new Point3D( 0.5, -0.5,  0.5)
    
    @nvert = @VList.length
    @PList = []
    for v in @VList
      @PList.push(new Point3D(v))
      
    ilist = [
         [0,1,2,3]
         [1,4,7,2]
         [4,5,6,7]
         [5,0,3,6]
         [0,1,4,5]
         [2,3,6,7]
    ]
    
    # hard coded number of 6 for a cube
    for i in [0...6]
      @addPolygon(i, ilist[i])
      
    r = Math.floor(Math.random()*128) + 127
    g = Math.floor(Math.random()*128) + 127
    b = Math.floor(Math.random()*128) + 127
    @setColor( {red: r, green: g, blue: b} )
    
    if _pos == null or _pos == undefined
      # default to display in middle of canvas
      x = window.innerWidth/2
      z = window.innerHeight/2
      @move(new Point3D(0))
    else if _pos.objName == 'Point3D'
      @move(_pos)