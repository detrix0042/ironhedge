window.roll_history =
  add: (obj) ->
    switch obj._type
      when 'D'
        @dice.hist.push(obj)
        @dice.idx += 1
      when 'J'
        @jewels.hist.push(obj)
        @jewels.idx += 1
    
  dice:
    hist: []
    idx: -1
  jewels:
    hist: []
    idx: -1

$(document).ready ->
  
  rolldice = (event) ->
    if typeof(event) == 'string'
      d = new Dice(event)
    else
      d = new Dice(event.data['dstr'])
    d.roll()
    Dice.display('#diceResultsView')
    
  rolljewels = (numj) ->
    j = new Jewels()
    j.roll(numj)
    Jewels.display('#diceResultsView')
    
  window.diceInputCheck = (event) ->
    if event.keyCode == 13
      if event.data.action == 'dice'
        dstr = event.currentTarget.value
        rolldice(dstr)
      else if event.data.action == 'jewel'
        jstr = event.currentTarget.value
        rolljewels(jstr)
      
  
  $('#diceInputStr').on('keyup', {'action': 'dice'}, diceInputCheck)
  $('#jewelInput').on('keyup', {'action': 'jewel'}, diceInputCheck)
  
  window.current_panel = $('#diceBtnPanel')
  
  $.get(
       '/dice/fetchDiceBtns'
  )
    .done((btns) ->
      panel = $('#diceBtnPanel')
      for b in btns
        btn = $('#diceBtn_template').clone()
        btn.text(b.label)
        btn.on('click', {'dstr': b.label}, rolldice)
        panel.append(btn)
  )
  
  showPanel = (event) ->
    db_tables = ['#creatureFilterPanel']

    window.current_panel.addClass('behidden')
    if event.data.panel not in db_tables
      panel =$(event.data.panel)
      panel.removeClass('behidden')
      window.current_panel = panel
      if event.data['panel'] == '#diceBtnPanel'
        obj = window.Dice
      else if event.data['panel'] == '#jewelInputPanel'
        obj = window.Jewels
      obj.display('#diceResultsView')
    else
      mainView = $(event.data.panel)
      mainView.removeClass('behidden')
      window.current_panel = mainView
      if event.data.panel == '#creatureFilterPanel'
        if not window.creature_panel
          window.creature_panel = new window.creature_table(mainView)
          creature_classes = $('li.klass_name')
          x = 0
        else
          window.creature_panel.fetch_race_select()
          
  
  $('#navItem_Dice').on('click', {'panel': '#diceBtnPanel'}, showPanel)
  $('#navItem_Jewels').on('click', {'panel': '#jewelInputPanel'}, showPanel)
  $('#navItem_Creatures').on('click', {'panel': '#creatureFilterPanel'}, showPanel)
  
  #window.selectRace = <%# render(partial: 'creatures/select_race') %>
