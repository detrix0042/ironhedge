# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

class window.creature_table
  constructor: (view) ->
    @mainView = view
    @creaturePanel = null
    @fetch_creatures()

  fetch_creatures: =>
    obj = this
    if not @$raceSelect
      $.get(
        'creatures/fetch_all'
      )
      .done((data) ->
        obj.creaturePanel = data
        obj.mainView.html(obj.creaturePanel)
        obj.bindEvents(obj)
        creature_klasses = $('.klass_name')
        x = 0
      )

  bindEvents: (self) ->
    $.get(
      'creatures/fetch_racelist'
    )
    .done((racelist) ->
      for race in racelist
        race = race.replace(/\s/g, '-')
        $("#ul_#{race}").click({'panel': race}, self.toggleShow)
    )
  
  toggleShow: (event) ->
    panel = $("#klass_panel_#{event.data.panel}")
    if panel.hasClass('open')
      panel.animate({height: 0}, 400).removeClass('open')
    else
      num = panel[0].children.length
      ele = panel.children()[0]
      h = $(ele).height() * num
      panel.animate({height: h}, 400).addClass('open')
      
      
class window.creatureView
  constructor: ->
    @klass_name = 'Men'
    @numof = '2D6'
    @elal = '1'
    @lp = '7'
    @af = 'Sw'
    @df = 'Sh'
    @armor = 'PA'
    @numatt = 1
    @kp = 5
    @mph = 7
    @money = 'D10s'
    @special = '3'
    @jewel = '5'
    @Str = 7
    @Int = 7
    @Dex = 7
    
  fetchCreature: (klass) ->
    $.get(
      '/creatures/fetch_creature',
      klass: klass
    )
    .done((view) ->
    
    )
