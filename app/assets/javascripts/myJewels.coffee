class window.Jewels
  constructor: ->
    @description = "Class to house AJAX api call to the backend for Jewel value determination."
    @_type = 'J'
    @jvalues = ['10', '20', '50', '100', '200', '500',
      '1000', '1500', '2000', '3000', '5000', '7000',
      '10000', '15000', '20000'
    ]
    @jtypes = ['di', 'em', 'rb', 'sa', 'am', 'op']

  roll: (jstr) ->
    d = new RND(20)
    jlen = @jvalues.length

    @vals = new Array(jlen).fill(0)
    @tps = new Array(jlen)
    for i in [0...jlen]
      @tps[i] = new Array(6).fill(0)

    @total = 0

    for i in [1..Number(jstr)]
      evens = 0
      while d.roll() % 2 == 0
        evens += 1

      if evens > 14
        evens = 14

      @vals[evens] += 1
      @total += Number(@jvalues[evens])
      tpi = @tp_idx(d.roll())
      @tps[evens][tpi] += 1
    
    window.roll_history.add(@)
    Jewels.display()

  tp_idx: (val) ->
    dx = 0
    if val in [1, 2]
      dx = 1
    else if val in [3..5]
      dx = 2
    else if val in [6..9]
      dx = 3
    else if val in [10..14]
      dx = 4
    else if val in [16..19]
      dx = 5

    return dx


  @display: (view_id) ->
    view = $(view_id)
    if not view
      view = $('#diceResultsView')
    templates = $('#templates')
    resultsView = $('#jewelResultsItem_template').clone()

    jcount_col = $("<div class='dflexc-jcs jcount_col'></div>")
    jvalue_col = $("<div class='dflexc-jcs jvalue_col'></div>")
    jtypes_col = $("<div class='dflexc-jcs jtypes_col flexfit'></div>")

    jhc = templates.find('#jewelHeader_count').clone()
    jhv = templates.find('#jewelHeader_value').clone()
    jht = templates.find('#jewelHeader_types').clone()
    jcount_col.append(jhc)
    jvalue_col.append(jhv)
    jtypes_col.append(jht)
    
    rh = window.roll_history
    if rh.jewels.idx == -1
      return
      
    j = rh.jewels.hist[rh.jewels.idx]

    vals = j.vals
    tpa = j.tps
    jval = 0
    row = 0
    for num in vals
      if num == 0
        jval += 1
        continue

      jhc = templates.find('#jewel_count').clone()
      jhv = templates.find('#jewel_value').clone()


      jhc.text("#{num}")
      jhv.text("#{j.jvalues[jval]}s")

      jcount_col.append(jhc)
      jvalue_col.append(jhv)

      jtype_t = templates.find('#jewel_types').clone()
      jtps = tpa[jval]

      for jt in [0..5]
        if jtps[jt] > 0
          dtp = templates.find("#jtype_#{j.jtypes[jt]}").clone()
          dtp.find("##{j.jtypes[jt]}_cnt").text("#{jtps[jt]}")
          jtype_t.append(dtp)
          if row % 2 == 0
            jhc.toggleClass('j_stripe1', true)
            jhv.toggleClass('j_stripe1', true)
            jtype_t.toggleClass('j_stripe1', true)
          else
            jhc.toggleClass('j_stripe2', true)
            jhv.toggleClass('j_stripe2', true)
            jtype_t.toggleClass('j_stripe2', true)

      jtypes_col.append(jtype_t)
      jval += 1
      row += 1

    resultsView.empty()
    resultsView.append(jcount_col)
    resultsView.append(jvalue_col)
    resultsView.append(jtypes_col)
    view.html(resultsView)
    $('#diceResultsStatus').html("Total: #{j.total}s")
