# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
class window.Dice
  constructor: (dstr) ->
    @Description = "Class to represent a dice roll"
    @_type = 'D'
    @left = 1
    @mid = 1
    @right = 6
    @parse(dstr)
    @minv = @mid * @right
    @total = 0
    @results = null

  parse: (dstr) ->

    dlims = " ;:/,.#-_=+@"
    dlimx = -1
    for ch in dlims
      if ch in dstr
        dlimx = dstr.indexOf(ch)
        break

    dstr = dstr.toUpperCase()
    if dlimx > -1
      @parse3(dstr, dlimx)
    else
      @parse2(dstr)

  parse3: (dstr, dlimx) ->
# parsing for format 10 2d6
# the 10 will be the left
# the 2 will be mid
# the 6 is the right
    console.log("parse3:")
    @left = Number(dstr.substring(0, dlimx))

    dstr = dstr.substr(dlimx+1)

    if dstr.indexOf('%D') >= 0
      dx = dstr.length - 2
      if dx > 0
        @mid = Number(dstr.substring(0, dx))
      @right = 100
      return

    dx = dstr.indexOf('D')
    @mid = Number(dstr.substring(0, dx))
    @right = Number(dstr.substr(dx+1))

  parse2: (dstr) ->
    dx = dstr.indexOf('D')

    if dstr.indexOf('%D') >= 0
      @right = 100
      if dstr == '%D'
        return

      @left = Number(dstr.substring(0, dx-1))
      return
    if dx > 0
      @left = Number(dstr.substring(0, dx))
    @right = Number(dstr.substr(dx+1))

  roll: ->
    rnd = new RND(@right)
    @total = 0

    minval = (@mid * @right) - (@mid - 1)
    @results = new Array(minval).fill(0)
    for l in [1..@left]
      subtotal = 0
      for m in [1..@mid]
        subtotal += rnd.roll()
      @results[subtotal] += 1
      @total += (subtotal + @mid)

    window.roll_history.add(@)

  @display: (view_id) ->
    results_item = $('#diceResultsItem_template')
    rh = window.roll_history
    if rh.dice.idx == -1
      return
      
    d = rh.dice.hist[rh.dice.idx]
    
    drv = $(view_id)
    if not drv
      drv = $('#diceResultsView')
      
    fh = results_item.height() + 2
    vh = drv.height()
    max_rows_per_col = parseInt(vh / fh) + 1
    rows_cnt = d.results.length
    num_cols = 1
    if rows_cnt > max_rows_per_col
      num_cols = parseInt(rows_cnt / max_rows_per_col)
      if (rows_cnt % max_rows_per_col) > 0
        num_cols += 1

    center = true
    if num_cols > 8
      center = false
    drv.toggleClass('dflexr-jcs', not center)
    drv.toggleClass('dflexr-jcc', center)

    item_template = $('#diceResultsItem_template')

    rcnt = 0
    cw_max = 0
    minv = parseInt(d.minval)
    drv.empty()
    for c in [0...num_cols]
      col = $('#diceResultsCol_template').clone().empty()
      col.toggleClass('diceResultsColumn', true)
      x = 0
      if rows_cnt <= max_rows_per_col
        x = rows_cnt
      else
        x = max_rows_per_col
        if num_cols > 1 and c == num_cols - 1
          x = rows_cnt % max_rows_per_col
          if x == 0
            x = max_rows_per_col
      for r in [0...x]
        itemplate = item_template.clone().empty()
        itemplate.html("#{rcnt + d.mid} --> #{d.results[rcnt]}")
        if d.results[rcnt] == 0
          itemplate.toggleClass('greyText', true)
          itemplate.toggleClass('blackText', false)
        else
          itemplate.toggleClass('blackText', true)
          itemplate.toggleClass('greyText', false)

        col.append(itemplate)
        rcnt += 1
      $('#diceResultsView').append(col)
      $('#diceResultsStatus').html("Total: #{d.total}")

    navPanelWidth = $('#mainNavPanel').width()
    dicePanleWidth = $('#diceBtnPanel').width()
    vWidth = window.innerWidth
