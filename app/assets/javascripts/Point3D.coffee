class window.Point3D
  objName: 'Point3D'
  
  constructor: (px, y = 0.0, z = 0.0) ->
    if typeof(px) == 'object' and px.objName == 'Point3D'
      @x = px.x
      @y = px.y
      @z = px.z
    else
      @x = px
      @y = y
      @z = z

  # px is either the x co-ordinate or a Point3D object
  set: (px, y, z) ->
    # if px is an object, assuming its a Point3D object
    if typeof(px) == 'object' and px.objName == 'Point3D'
      @x = px.x
      @y = px.y
      @z = px.z
    else
      @x = px
      @y = y
      @z = z

  add: (px, y, z) ->
    if typeof(px) == 'object' and px.objName == 'Point3D'
      new Point3D(@x + px.x, @y + px.y, @z + px.z)
    else
      new Point3D(@x + px, @y + y, @z + z)

  sub: (px, y, z) ->
    if typeof(px) == 'object' and px.objName == 'Point3D'
      new Point3D(@x - px.x, @y - px.y, @z - px.z)
    else
      new Point3D(@x - px, @y - y, @z - z)

  equals: (p) ->
    (@x == p.x) && (@y == p.y) && (@z == p.z)

  # calculation of distance between two 3D points
  @_dist: (p1, p2) ->
    dx = p1.x - p2.x
    dy = p1.y - p2.y
    dz = p1.z - p2.z
    
    Math.sqrt(dx * dx + dy * dy + dz * dz)

  # arguments have to be Point3D points
  @distance: (p1, p2) ->
    if p2
      # if two points are given
      d = @_dist(p1, p2)
    else
      # if only one point is given the distance
      # between this point and point given
      d = @_dist(this, p1)
    return d
    
  @normalize: (p1, p2=null) ->
    if p2
      dx = p1.x - p2.x
      dy = p1.y - p2.y
      dz = p1.z - p2.z
      rho = @_dist(p1, p2)
    else
      dx = p1.x - @x
      dy = p1.y - @y
      dz = p1.z - @z
      rho = @_dist(this, p2)
      
    new Point3D(dx/rho, dy/rho, dz/rho)

  toString: ->
    "(X:#{@x}, Y:#{@y}, Z:#{@z})"
