class window.RND

  constructor: ->
    @rng = 0
    @seed = 0.0
    self = this # this was need for the constuctor; other functions can use @(this)

    if arguments.length > 2
      alert("in RND constructor: too many arguments given: #{arguments.length}")
      return

    ck_arg = (_arg) ->
# check if arg is int or float
      arglen = arguments.length
      if arg == +arg && arg == (arg | 0) # check if int
        console.log("setting range")
        self.rng = arg
        self.setSeedTime() if arglen == 1
      else
        if arg == +arg && arg != (arg | 0) # check if float
          console.log("setting seed")
          self.rng = 6
          self.seed = arg if arglen == 1

    if arguments.length == 0
      self.setSeedTime()
    else
      for arg in arguments
        ck_arg(arg)

  setSeedTime: () ->
    t = Date.now()
    cnt = 0
    while t > 1
      t = t / 100
      cnt += 1

    sd = 0.0
    for i in [0...cnt]
      t = t * 100
      t1 = Math.floor(t)
      t = t - t1
      sd = (sd + t1) / 100
    @seed = sd

  seed_snap: ->
    s = Math.abs((Math.pow(Math.PI, @seed) - Math.log(@seed)))
    s *= 1000000
    @seed = s - Math.floor(s)

  roll: (rng) ->
    if rng
      q = @seed * rng
    else
      q = @seed * @rng
    @seed_snap()
    return Math.floor(q)
