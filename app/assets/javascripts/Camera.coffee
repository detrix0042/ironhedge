class window.Viewport
  constructor: (w=640, h=320) ->
    @width = w
    @height = h

class window.Camera
  objName: "Camera"
  
  constructor: (cp=null, front=null, top=null, right=null, zoom=1.0) ->
    if cp == null
      @pos = new Point3D( 0.0, 0.0, 5.0)
    else
      @pos = new Point3D(cp)
      
    if front == null
      @front = new Point3D(0.0, 0.0, -1.0)
    else
      @front = new Point3D(front)

    if top == null
      @top = new Point3D(0.0, 1.0, 0.0)
    else
      @top = new Point3D(top)

    if right == null
      @right = new Point3D(1.0, 0.0, 0.0)
    else
      @right = new Point3D(right)

    @zoom = zoom

  # wc is widht or canvas element
  setViewport: (wc=640, h=320) ->
    if @Viewport == null or @Viewport == undefined
      @Viewport = new Viewport()
    if typeof wc == 'number'
      @Viewport.width = wc
      @Viewport.height = h
    else
      @Viewport.width = wc.width
      @Viewport.height = wc.height

    
  setPos: (px, y=0, z=0) ->
    if typeof px == 'object' and px.objName == 'Point3D'
      @pos.x = p.x
      @pos.y = p.y
      @pos.z = p.z
    else
      @pos.x = px
      @pos.y = y
      @pos.z = z

  setFront: (px, y=0, z=0) ->
    if typeof px == 'object' and px.objName == 'Point3D'
      @front.x = p.x
      @front.y = p.y
      @front.z = p.z
    else
      @front.x = px
      @front.y = y
      @front.z = z

  setTop: (px, y=0, z=0) ->
    if typeof px == 'object' and px.objName == 'Point3D'
      @top.x = p.x
      @top.y = p.y
      @top.z = p.z
    else
      @top.x = px
      @top.y = y
      @top.z = z

  setRight: (px, y=0, z=0) ->
    if typeof px == 'object' and px.objName == 'Point3D'
      @Right.x = p.x
      @Right.y = p.y
      @Right.z = p.z
    else
      @Right.x = px
      @Right.y = y
      @Right.z = z

  move: (px, y=0, z=0) ->
    if typeof px == 'object' and px.objName == 'Point3D'
      @pos.x += p.x
      @pos.y += p.y
      @pos.z += p.z
    else
      @pos.x += px
      @pos.y += y
      @pos.z += z
  
  # rotated about the z axis in radians (2pi is full circle)
  rotateZ: (ra) ->
    # calculate sine and cosine values once for efficiencies
    ca = Math.cos(ra)
    sa = Math.sin(ra)
    
    front.x = front.x * ca + right.x * sa
    front.y = front.y * ca + right.y * sa
    front.z = front.z * ca + right.z * sa

    right.x = right.x * ca + front.x * sa
    right.y = right.y * ca + front.y * sa
    right.z = right.z * ca + front.z * sa
    
  toString: ->
    "Camera:\n" +
    "\tPosition: #{@pos.toString()}\n" +
    "\tFront:    #{@front.toString()}\n" +
    "\tTop:      #{@top.toString()}\n" +
    "\tRight:    #{@Right.toString()}"
    