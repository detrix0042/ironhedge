class window.Polyhedron
  objName: 'Polyhedron'
  
  constructor: (ctx) ->
    @context = ctx
    if window.jworld
      @World = window.jworld
    else
      @world = new World()
      
    @nvert = 0 # number of verticies
    @npoly = 0 # number of polygons
    @IList = [] # index list into verticie list
    # VList is static definition; do not alter
    @VList = [] # vertex list
    # projection list
    # project VList to PList
    # PList is the results of calculations done to VList
    @PList = []
    # list of polygons
    @pgon = []
    @pos = new Point3D(0, 0, 0)
    @angle = {x: 0, y: 0, z: 0}
    @size = 1
    @color = {red: 0, green: 0, blue: 0}
    
    
  # add a verticie (vp) to list of verticies
  # at vindex
  addVert: (vindex, vp) ->
    if @VList == null
      @VList = []
    if vindex < @nvert
      @VList[vindex] = vp
    else
      alert("addVert: attempt to add a verticie greater than #{@nvert}\nA polyhedron has a specific number of \ +
       \verticies")
      
  # add a polygon(face) to this object
  # npoly is index into pgon array
  # ilist is array of indexes into VList
  addPolygon: (npoly, ilist) ->
    @IList[npoly] = ilist
    if @pgon == null
      @pgon = []
      
    pgn = @pgon[npoly] = new Polygon(ilist, @PList)
    pgn.setColor(@color)
    pgn.size = @size
    
  # list is a array of Point3D object
  setVList: (list) ->
    @VList = vlist
    for p in @VList
      @PList.push(new Point3D(p))

  setSize: (s) ->
    @size = s
    for pg in @pgon
      pg.setSize(s)
      
  # for now c is a json of { red: ??, green: ??, blue: ?? }
  # there may be more than a single argument. This is the only way I could
  # figure out how to basically overload this function
  # 1 argument is the json shown above
  # 2 arguments c is still json as shown above, and argument 2 is index to which polygon gets the color
  # 4 arguments: argument[0] is red, [1] is green, [2] is blue, and [3] is index of polygon
  setColor: (c) ->
    if arguments.length == 1
      if typeof c == 'object'
        @color.red = c.red
        @color.green = c.green
        @color.blue = c.blue
        for pg in @pgon
          pg.setColor(c)
    else if arguments.length == 2
      @pgon[arguments[1]].setColor(c)
    else if arguments.length == 4
      c = { red: arguments[0], green: arguments[1], blue: arguments[2] }
      @pgon[arguments[3]].setColor(c)
    
  # set the position of light source for this polyhedron
  setLight: (p) ->
    @light = new Point3D(p)
    
  # dp is a Point3D object that is added to current position of this object
  move: (dp) ->
    @pos.x += dp.x
    @pos.y += dp.y
    @pos.z += dp.z

  toRad: (ang) ->
    (Math.PI/180) * ang
    
  # ang is angle in radians
  Rz: (ang) ->
    if typeof ang == 'string'
      ang = parseInt(ang)
      if ang == 0
        return
    rad = @toRad(ang)
    @angle.z += rad
    # 2pi is max
    if @angle.z > Math.PI*2
      @angle.z -= Math.PI*2
    
    ca = Math.cos(@angle.z)
    sa = Math.sin(@angle.z)
    
    for i in [0...@nvert]
      p = @PList[i]
      v = @VList[i]
      p.x = v.x*ca - v.y*sa
      p.y = v.x*sa + v.y*ca
     
  Ry: (ang) ->
    if typeof ang == 'string'
      ang = parseInt(ang)
      if ang == 0
        return
    rad = @toRad(ang)
    @angle.y += rad
    if @angle.y > (Math.PI * 2)
      @angle.y -= (Math.PI * 2)
      
    ca = Math.cos(@angle.y)
    sa = Math.sin(@angle.y)
    
    for i in [0...@nvert]
      p = @PList[i]
      v = @VList[i]
      p.x = v.x*ca + v.z*sa
      p.z = v.z*ca - v.x*sa

  Rx: (ang) ->
    if typeof ang == 'string'
      ang = parseInt(ang)
      if ang == 0
        return
    rad = @toRad(ang)
    @angle.x += rad
    if @angle.x > (Math.PI * 2)
      @angle.x -= (Math.PI * 2)

    ca = Math.cos(@angle.x)
    sa = Math.sin(@angle.x)

    for i in [0...@nvert]
      p = @PList[i]
      v = @VList[i]
      p.y = (v.y*ca - v.z*sa)
      p.z = (v.y*sa + v.z*ca)

  rotate: (r) ->
    @Ry(r.y) if r.y != 0
    @Rx(r.x) if r.x != 0
    @Rz(r.z) if r.z != 0
      
  deres: ->
    for pg  in pgon
      pg.deres()
    @VList = []
    @PList = []
    @IList = []
    @pgon = []
    @light = null
    @pos = null
    @angle = null
  
  # ctx is canvas context for drawing
  # cam is camera position/orientation
  draw: () ->
    if @nvert == 0
      @nvert = @VList.length
    if @World
      cam = @World.cam
      light = @World.light
      
      
    x1 = 0.0
    y1 = 0.0
    z1 = 0.0
    
    for p in @PList
      x1 = p.x + (@pos.x-cam.pos.x)
      y1 = p.y + (@pos.y-cam.pos.y)
      z1 = p.z + (@pos.z + cam.pos.z) # adding cam.pos.z because cam.z is negitive
  
      p.x = (x1*cam.right.x) + (y1*cam.right.y) + (z1*cam.right.z)
      p.y = (x1*cam.top.x) + (y1*cam.top.y) + (z1*cam.top.z)
      p.z = (x1*cam.front.x) + (y1*cam.front.y) + (z1*cam.front.z)
  
    d = []
    _d = []
    di = []
    
    for pg in @pgon
      pg.calcPos()
      d.push(Point3D.distance(cam.pos, pg.pos))
      _d.push(Point3D.distance(cam.pos, pg.pos))
      
    _d.sort((a, b) -> b-a )
    for i in [0...d.length]
      ii = d.findIndex((a) -> a == _d[i])
      di.push(ii)
      d[ii] = 'x'
      
      
    cnt = 0
    high = 0


    for i in di
      @pgon[i].draw(@context, cam, light, @size) if @pgon[i] != null
    
#    @context.fill()
#    @context.stroke()
      