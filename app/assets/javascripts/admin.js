//= require angular
//= require angular-resource

function htmlbodyHeightUpdate(){
    var height3 = $( window ).height();
    console.log("window height: " + height3 + "  (height3)");
    var height1 = $('#admin-nav').height();
    console.log("#admin-nav height: " + height1 + "  (height1)");
    var height2 = $('#admin-index-panel').height();
    console.log("#admin-index-panel height: " + height2 + "  (height2)");

    var ugcp = $('#admin-usergroup-panel');
    var padTop = parseInt(ugcp.css('top')) - $('#topNavBar').height();
    var padBot = parseInt(ugcp.css('bottom'));

    var padadj = (padTop + padBot);
    console.log("padding adjust = " + padadj +"px");
    console.log('#admin-usergroup-panel height: ' + ugcp.height());

    $('#adminCtrlPanel').css({'height': (ugcp.height() + padadj)});

    if(height2 > height3){
        console.log("height2 > height3");
        $('html').height(Math.max(height1,height3,height2)+10);
        $('body').height(Math.max(height1,height3,height2)+10);
    }
    else
    {   console.log("height 2 < height3");
        console.log("height1 = " + height1);
        console.log("height2 = " + height2);
        console.log("height3 = " + height3);
        $('html').height(Math.max(height1,height3,height2));
        //$('body').height(Math.max(height1,height3,height2));
    }    
}

function toggleAdminSidebarSelected(clk, panel) {
    var sel = $('.sidebar-nav-active').toggleClass('sidebar-nav-active', false);
    $(clk).toggleClass('sidebar-nav-active', true);
}

function findObjIndex(id, list) {
    var i = 0, Obj;
    for(;i<list.length; i+=1) {
        Obj = list[i];
        if(Obj.id == id)
            break;
    }
    return i;
}







/*
 * ---------------- Angular methods below
*/

var adminModule = angular.module("adminModule", []);

adminModule.controller("adminPanelController", ['$scope', function($scope) {
        $scope.adminSelectedPanel = 'usergroup';
        $scope.PanelCtrl = { 
            main: "usergroup",
            iron: "index" 
            };
        }]);

/**
 * A generic confirmation for risky actions.
 * Usage: Add attributes: ng-really-message="Are you sure"? ng-really-click="takeAction()" function
 */
adminModule.directive('ngReallyClick', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                var message = attrs.ngReallyMessage;
                if (message && confirm(message)) {
                    scope.$apply(attrs.ngReallyClick);
                }
            });
        }
    }
}]);

adminModule.controller("adminUserListCtrl", ['$scope', '$http', function(scope, http) {
    if(scope.userlist == undefined || scope.userlist == null) {
        http.get('utility/fetchUserList').success(function(data) {
            scope.userlist = data;
        });
    }
    scope.removeUser = function(id) {
            var AUTH_TOKEN = $('meta[name=csrf-token]').attr('content');
            http.delete('utility/removeUser/' + id, {params: {authenticity_token: AUTH_TOKEN}} )
                .success(function(ack) {
                    if(ack === 'deleted') {
                        idx = findObjIndex(id, scope.userlist);
                        scope.userlist.splice(idx, 1);
                        $('#wellforUser'+id).remove();
                    }
                })
    }
}]);

adminModule.controller("adminGroupsCtrl", ['$scope', '$http', function(scope, http) {
    scope.fetchGroupList = function() {
        http.get('utility/fetchGroupList').success(function(data) {
        scope.grouplist = data;
        });
    }

    if(scope.grouplist == undefined || scope.grouplist == null) {
        scope.fetchGroupList();
    }

    scope.addNewGroup = function() {
        var name = $('#newGroupName').val();
        var AUTH_TOKEN = $('meta[name=csrf-token]').attr('content');
        var prams = { groupname: name, authenticity_token: AUTH_TOKEN}
        http.put('utility/addNewGroup/', prams).
            success(function(newList) {
                if(newList != 'undefined' || newList != null) {
                    scope.grouplist = newList;
                }
                $('#newGroupName').val("");
            })
    }


    scope.removeGroup = function(gid) {
        var AUTH_TOKEN = $('meta[name=csrf-token]').attr('content');
        http.delete('utility/removeGroup/' + gid, { params: {authenticity_token: AUTH_TOKEN }} )
                .success(function(ack) {
                    if(ack === 'deleted') {
                        var idx = findObjIndex(gid, scope.grouplist);
                        scope.grouplist.splice(idx, 1);
                        $('#wellforGroup'+id).remove();
                        //var el = $('#wellforUser'+id);
                        //el.remove();
                    }
                })
    }

}]);

adminModule.controller("ironAdminTableCtrl", ['$scope', '$http', '$compile', function(scope, http, compile) {
    scope.ironPanelCtrl = {
        adminTable: 'tableIndex'
    };
    
    scope.getIronhedgeClassAdminTable = function(klass) {
        http.get('utility/fetch' + klass + 'AdminTable')
            .success(function(data) {
                scope.PanelCtrl['iron'] = 'tables';
                var tableIndex = $("#ironhedgeTableIndex");
                var $data = $(data);
                compile($data)(scope);

                var w = $("#admin-ironhedge-panel").width() - 30;
                $data.css("width", w+"px");
                tableIndex.after($data);
                scope.ironPanelCtrl['adminTable'] = 'admin' + klass + 'Table';
            });

    };

    scope.deleteIronClass = function(type, rank) {
        var AUTH_TOKEN = $('meta[name=csrf-token]').attr('content');
        http.delete('utility/deleteIronClass', 
            { params: { type: type,
              rank: rank,
              authenticity_token: AUTH_TOKEN }})
            .success(function(data) {
                if(data['ack'] === "delete successful") {
                    alert(type + " Class: " + rank + " deleted successfully");
                    $('#'+data['elid']).remove();
                }
            });
    };

    
    
    scope.getSavingRollsAdminTable = function() {
        http.get('utility/fetchSavingRollList')
            .success(function(data) {
                scope.savingRollList = data; 
            });

        http.get('utility/fetchSavingRollsAdminTable')
            .success(function(data) {
                scope.PanelCtrl['iron'] = 'tables';
                var tableIndex = $("#ironhedgeTableIndex");
                var $data = $(data);
                compile($data)(scope);

                var w = $("#admin-ironhedge-panel").width() - 30;
                $data.css("width", w+"px");
                tableIndex.after($data);
                scope.ironPanelCtrl['adminTable'] = 'savingRollTable'
            });
    };

    scope.sr = {  id: 0,
                  sid: "",
                  c1: 0,
                  c2: 0,
                  c3: 0,
                  c4: 0,
                  c5: 0
    };
    scope.srAdd = function(srrow) {
        console.log("srrow =" + srrow);
        scope.sr = angular.copy(srrow);
        var AUTH_TOKEN = $('meta[name=csrf-token]').attr('content');
        http.post('utility/srAdd', { authenticity_token: AUTH_TOKEN, srAdd: scope.sr })
            .success(function(data) {
                if(data)
                    scope.savingRollList.push(data);
            });
    };

    scope.srDelete = function(sr) {
        console.log("srDelete:");
        console.log("sr = " + sr);
        console.log("sr.id = " + sr.id);
        var AUTH_TOKEN = $('meta[name=csrf-token]').attr('content');
        var pm = { params: { authenticity_token: AUTH_TOKEN, 
                             srid: sr.id }};
        http.delete('utility/srDelete', pm)
            .success(function(data) {
                if(data === "delete successful") {
                    var idx = findObjIndex(sr.id, scope.savingRollList);
                    scope.savingRollList.splice(idx, 1);
                }

            });
    };

    scope.backToTableIndex = function(from) {
        felement = $('#' + from + "_admintable");
        scope.ironPanelCtrl['adminTable'] = 'tableIndex';
        scope.PanelCtrl['iron'] = 'index'
        felement.remove();

    };
}]); 

// #####################################################     SavingRollCtrl

adminModule.controller("SavingRollCtrl", ['$scope', '$http', '$compile', function(scope, http, compile) {
    scope.savingRollEditInline = function(srid) {
        http.get('utility/fetchSavingRollInline', { srid: srid })
            .success(function(data) {
                
            });
    };

}]);

// ====================================================   end of Angular controller defitions

$(document).ready(function () {
    htmlbodyHeightUpdate();
    $( window ).resize(function() {
        htmlbodyHeightUpdate();
    });
    $( window ).scroll(function() {
        //height2 = $('.main').height();
        htmlbodyHeightUpdate();
    });

    //adminModule = angular.module("adminModule", []);
});

