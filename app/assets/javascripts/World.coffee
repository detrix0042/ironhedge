class window.World
  objName: 'World'
  
  constructor: (cam, light) ->
    if cam and cam.objName == 'Camera'
      @cam = cam
    else
      @cam = new Camera()
      
    if light and light.objName == 'Point3D'
      @light = light
    else
      @light = new Point3D(0, -50, -10)