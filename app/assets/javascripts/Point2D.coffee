class window.Point2D
  objName: 'Point2D'
  
  constructor: (px, y=0.0) ->
    if typeof(px) == 'object' and px.objName == 'Point3D'
      @x = px.x
      @y = px.y
    else
      @x = px
      @y = y

  equals: (pnt) ->
    if typeof(pnt) != typeof(this)
      return

    (@x == pnt.x) && (@y == pnt.y)

  toString: ->
    "(X:#{@x}, Y:#{@y})"
