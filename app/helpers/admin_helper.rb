module AdminHelper
  def allothergroups(user)
    allgroups = Group.alphasort.collect { |g| g.name }
    usergroups = user.groups.collect { |g| g.name }
    allgroups - usergroups
  end

  def getGroupID(gname)
    g = Group.find_by_name(gname)
    g.id
  end
end
