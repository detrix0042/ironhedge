require 'rails/all'
require 'logging'
logfilename = 'dice_error.log'
Logging.logger['Dice'].level = :debug
logfile = "../../log/#{logfilename}"
if Rails.root.present?
  logfile = Rails.root.join('log', logfilename)
end
Logging.logger['Dice'].add_appenders(
    Logging.appenders.stdout,
    Logging.appenders.file(logfile)
)

require_relative 'rnd'

class Dice
  @results = []
  @left = 1
  @mid = 1
  @right =6
  
  def initialize(dstr)
    @log = Logging.logger[self]

    parse dstr
    @log.info("<left:#{@left} mid:#{@mid} right:#{@right}> results: #{@results}")
    @rnd = RND.new @right
    roll
  end
  
  def to_json
    j = {
      total: @results[0],
      minval: @mid,
      results: @results[(1..-1)]
    }
  end
  
  private
  def parse(dstr)
    di = dstr.index('D')
    if di == nil
      di = dstr.index('d')
    end

    if di.nil?
      @log.error("parse: Error -- dstr did not contain 'd' or 'D'")
    else
      @log.info("parse: #{dstr}")
    end
    
    if dstr.include? %w[%d %D]
      @left = 1
      @mid = 1
      @right = 100
      return
    end
  
    rstr = dstr[di+1..-1]
    lstr = dstr[0...di]
    delx = check_delim lstr
    
    @right = rstr.to_i
    if di == 0
      @left = 1
      @mid = 1
      return
    end
    if delx
      @left = lstr[0...delx].to_i
      @mid = lstr[delx+1..-1].to_i
    else
      @left = lstr.to_i
      @mid = 1
    end
  end
  
  def check_delim(lstr)
    if lstr.length < 3
      return nil
    end
    
    for ch in ' ;:/|\\'.split("")
      idx = lstr.index(ch)
      if idx
        return idx
      end
    end
  end
  
  def roll
    @results = [0] * ((@mid * @right) - (@mid - 1) + 1)
    for i in (0...@left) do
      subt = 0
      for j in (0...@mid) do
        subt += (@rnd.roll) + 1
      end
      @results[subt - (@mid-1)] += 1
      @results[0] += subt
    end
    x = 0
  end
end
