require 'rails/all'
require 'logging'
logfilename = 'jewel_error.log'
Logging.logger['Jewel'].level = :debug
logfile = "../../log/#{logfilename}"
if Rails.root.present?
  logfile = Rails.root.join('log', logfilename)
end
Logging.logger['Jewel'].add_appenders(
  Logging.appenders.stdout,
  Logging.appenders.file(logfile)
)

require_relative 'rnd'

class Jewel
  
  def initialize(cnt)
    @log = Logging.logger[self]
    @rnd = RND.new 10
    
    @log.info("#{cnt} Jewels being determined")

    @results = [0] * 15
    @types = Array.new 15
    @types.map! { |x| x = [0] * 6 }
    @total = 0
    
    run cnt.to_i
  end
  
  def run cnt
    jvalues = [10, 20, 50, 100, 200, 500, 1000, 1500, 2000, 3000, 5000, 7000, 10000, 15000, 20000]
    for i in (0...cnt)
      evens = 0
      while ((@rnd.roll) % 2) == 0
        evens += 1
      end
      @log.info("number of evens: #{evens}")
      if evens > 14
        evens = 14
        @log.info("more that 14 evens!!!\nclipping evens to 14")
      end
      @results[evens] += 1
      @total += jvalues[evens]
      tp = @rnd.roll(20)
      tpi = tp_index tp
      @types[evens][tpi] += 1
    end
  end
  
  def to_json
    {
      vals: @results,
      tps: @types,
      total: @total
    }
  end
  
  private
  def tp_index(tp)
    idx = 0
    if tp == 1 or tp==2
      idx = 1
    elsif tp.between?(3, 5)
      idx = 2
    elsif tp.between?(6, 9)
      idx = 3
    elsif tp.between?(10, 14)
      idx = 4
    elsif tp.between?(16, 19)
      idx = 5
    end
    idx
  end
  
end
