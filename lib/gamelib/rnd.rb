class RND
  @range = 6
  @seed  = 0.0
  
  def initialize(r = 6, s = 0.0)
    if s == 0.0
      @seed = makeseed_time
    else
      @seed = s
    end
    
    @range = r
    salt
    
  end
  
  def roll(r=nil)
    if r
      q = @seed * r
    else
      q = @seed * @range
    end
    @seed = nextseed
    q.to_i
  end
  
  
  private
  def makeseed_time
    t = Time.now.to_f

    cnt = 4
    until t < 100
      t /= 100
      cnt += 1
    end

    s = 0.0
    (0...cnt).each do
      s = s + t.to_i
      s /= 100
      t = t - t.to_i
      t *= 100
    end
    s
  end
  
  def nextseed
    seed = ((Math::PI**@seed) - Math.log2(@seed))
    if seed < 0
      seed *= -1
    end
    seed *= 1000000
    seed -= seed.to_i
  end
  
  def salt
    x = roll(10000)
    (0..x).each do
      nextseed
    end
  end
end