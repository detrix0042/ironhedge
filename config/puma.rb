rails_env = ENV['RAILS_ENV'] || "development"
# Change to match your CPU core count
if rails_env == 'production'
  workers 1
  threads 1, 2
else
  workers = 4
  threads = 4, 8
  
end


# Min and Max threads per worker

app_dir = File.expand_path("../..", __FILE__)
shared_dir = "#{app_dir}/shared"

# Default to production

environment rails_env
puts "running in #{rails_env}"

# Set up socket location
#bind "unix://#{shared_dir}/sockets/puma.sock"
if rails_env == 'production'
  bind "unix:/opt/www/hedge/iron.sock"
end

# Logging
stdout_redirect "#{shared_dir}/log/puma.stdout.log", "#{shared_dir}/log/puma.stderr.log", true

# Set master PID and state locations
pidfile "#{shared_dir}/pids/puma.pid"
state_path "#{shared_dir}/pids/puma.state"
activate_control_app

on_worker_boot do
  require "active_record"
  ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
  ActiveRecord::Base.establish_connection(YAML.load_file("#{app_dir}/config/database.yml")[rails_env])
end
