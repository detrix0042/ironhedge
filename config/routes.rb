Rails.application.routes.draw do
  # get 'creatures/index'
  #
  # get 'creatures/fetch_race'
  #
  # get 'creatures/fetch_creature'
  #
  # get 'dice/roll'

  root 'main#index'
  
  get 'main/index'
  
  get 'login',       to: 'main#login'
  post 'login',      to: 'main#login'
  get 'logout',      to: 'main#logout'

  get  'signup',     to: 'main#signup'
  post 'signup',     to: 'main#signup'

  get 'admin',                to: 'admin#index'
  get 'admin/ironhedge',      to: 'admin#ironhedge',      as: 'ironhedge_edit'
  get 'admin/usergroup_edit', to: 'admin#usergroup_edit', as: 'usergroup_edit'

  post 'showlogin',  to: 'utility#showlogin'

  get   'utility/fetchUserList',    to: 'utility#fetcthUserList', as: 'userList'
  delete 'utility/removeUser/:id',  to: 'utility#removeUser',     as: 'removeUser'
  get   'utility/showEditUser/:id', to: 'utility#showEditUser',   as: 'showEditUser'
  patch 'utility/updateUser/:id',   to: 'utility#updateUser',     as: 'updateUser'

  post  'utility/addGroupToUser',        to: 'utility#addGroupToUser',      as: 'addGroupToUser'
  post  'utility/removeGroupFromUser',   to: 'utility#removeGroupFromUser', as: 'removeGroupFromUser'

  get   'utility/fetchGroupList',     to: 'utility#fetchGroupList',  as: "fetchGroupList"
  put  'utility/addNewGroup',         to: 'utility#addNewGroup',     as: 'addNewGroup'
  delete 'utility/removeGroup/:id',   to: 'utility#removeGroup',     as: 'removeGroup'

  get   'utility/fetchFighterAdminTable',    to: 'utility#fetchFighterAdminTable', as: 'fighterAdminTable'
  post  'utility/addFighterClass',           to: 'utility#addFighterClass' 
  patch 'utility/updateFighterClass/:id',    to: 'utility#updateFighterClass'
  get   'utility/editFighterClass/:id',      to: 'utility#editFighterClass'

  get   'utility/fetchThiefAdminTable', to: 'utility#fetchThiefAdminTable', as: 'thiefAdminTable'
  post  'utility/addThiefClass',        to: 'utility#addThiefClass' 
  patch 'utility/updateThiefClass/:id', to: 'utility#updateThiefClass'
  get   'utility/editThiefClass/:id',   to: 'utility#editThiefClass'

  get   'utility/fetchMagicianAdminTable', to: 'utility#fetchMagicianAdminTable', as: 'magicianAdminTable'
  get   'utility/editMagicianClass/:id',   to: 'utility#editMagicianClass'
  post  'utility/addMagicianClass',        to: 'utility#addMagicianClass'
  patch 'utility/updateMagicianClass/:id', to: 'utility#updateMagicianClass'

  get   'utility/fetchAlchemistAdminTable', to: 'utility#fetchAlchemistAdminTable', as: 'alchemistAdminTable'
  get   'utility/editAlchemistClass/:id',   to: 'utility#editAlchemistClass'
  post  'utility/addAlchemistClass',        to: 'utility#addAlchemistClass'
  patch 'utility/updateAlchemistClass/:id', to: 'utility#updateAlchemistClass'

  get   'utility/fetchPriestAdminTable', to: 'utility#fetchPriestAdminTable', as: 'priestAdminTable'
  get   'utility/editPriestClass/:id',   to: 'utility#editPriestClass'
  post  'utility/addPriestClass',        to: 'utility#addPriestClass'
  patch 'utility/updatePriestClass/:id', to: 'utility#updatePriestClass'
  
  delete 'utility/deleteIronClass'#,       to: 'utility#deleteIronClass'
  get    'utility/fetchSavingRollList'
  get    'utility/fetchSavingRollsAdminTable'

  get    'utility/fetchSavingRollInline/:srid', to: 'utility#srEditInline', as: 'srEditInline'
  post   'utility/srAdd', as: 'srAdd'
  delete 'utility/srDelete'
  
  ######################################################################################################3
  # dice api routes
  #################################
  
  get 'dice/fetchDiceBtns', to: 'dice#fetchDiceBtns'
  post 'dice/roll', to: 'dice#roll'

  ######################################################################################################3
  # jewel api routes
  #################################
  
  post 'jewel/cut', to: 'jewel#cut'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :admin do
    get 'index',               to: 'index'
    get 'fighterClassTable',   to: 'fighterClassTable'
  	get 'thiefClassTable',     to: 'thiefClassTable'
    get 'magicianClassTable',  to: 'magicianClassTable'
    get 'alchemistClassTable', to: 'alchemistClassTable'
    get 'priestClassTable',    to: 'priestClassTable'
  end
  
  ####################################
  # Creatures api
  ####################################
  get 'creatures/index',        to: 'creatures#index'
  get 'creatures/fetch_all',    to: 'creatures#fetch_all'
  get 'creatures/fetch_racelist'
  
  
  
 #match ':controller(/:action)(/:id)', via: [ :get,:post, :patch ] 
end
